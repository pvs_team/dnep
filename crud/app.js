// require "config" files
require('dotenv').config(
    {
        debug: process.env.DEBUG,
        path: __dirname + '/config/.env'
    });
var createError = require('http-errors'),
    express = require('express'),
    path = require('path'),
    cookieParser = require('cook' + 'ie-parser'),
    logger = require('morgan');
// express

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


// middleware
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views/back/dist')));
// require boot
require('./boot/database');
require('./models/userModel');
require('./boot/passport');
// require and use routs
const backRoutes = require('./routes/back');
app.use(backRoutes);
//require('./routes/front')(app);
// var routPath = require('path').join(__dirname, 'routes');
// require('fs').readdirSync(routPath, app).forEach(function (file) {
//     require('./routes/' + file)(app);
// });
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
