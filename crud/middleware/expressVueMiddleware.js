var expressVue = require("express-vue");
const path = require('path');
const vueOptions = {
    rootPath: path.join(__dirname, '../views/back/src'),
    vueVersion: "2.3.4",
    template: {
        html: {
            start: '<!DOCTYPE html><html>',
            end: '</html>'
        },
        body: {
            start: '<body>',
            end: '</body>'
        },
        template: {
            start: '<div id="app">',
            end: '</div>'
        }
    },
    head: {
        title: 'Hello this is a global title',
        scripts: [
            { src: 'https://example.com/script.js' },
        ],
        styles: [
            { style: '/assets/rendered/style.css' }
        ]
    }
};
module.exports = expressVueMiddleware = expressVue.init(vueOptions);