//const mongoose = require('mongoose');
const passport = require('passport');
const LocalPassport = require('passport-local');

const userModel = require('../models/userModel');

passport.use(new LocalPassport({
    usernameField: 'user[email]',
    passwordField: 'user[password]',
}, (email, password, done) => {
    userModel.findOne({email: email})
        .then((user) => {
            if (!user || !user.validatePassword(password)) {
                return done(null, false, {errors: {'email or password': 'is invalid'}});
            }
            return done(null, user);
        }).catch(done);
}));