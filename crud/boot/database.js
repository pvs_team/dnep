//Create connections
var mongoose = require('mongoose');
var mongoDB = 'mongodb://' + process.env.DB_HOST + ':' + process.env.DB_HOST + '/' + process.env.DB_NAME;
mongoose.connect(mongoDB,
    {
        useNewUrlParser: true
    });
/**
 * https://mongoosejs.com/docs/promises.html
 */
mongoose.Promise = global.Promise;
var db = mongoose.connection;
//Check connection error
db.on('error', console.error.bind(console, 'mongoDB connection error:'));
//Export module
module.exports = db;