// require
const express = require('express');
const router = express.Router();
const auth = require('./auth');
/*
Department controller routs
 */
const departmentsController = require('../controllers/back/departmentsController');
router.post('/api/department', departmentsController.store);
router.put('/api/department/:id', departmentsController.update);
router.get('/api/departments', departmentsController.getAll);
router.get('/api/department/:id', departmentsController.get);
router.delete('/api/department/:id', departmentsController.delete);
/*
Position controller routs
 */
const positionsController = require('../controllers/back/positionsController');
router.post('/api/position', positionsController.store);
router.put('/api/position/:id', positionsController.update);
router.get('/api/positions', positionsController.getAll);
router.get('/api/position/:id', positionsController.get);
router.delete('/api/position/:id', positionsController.delete);
/*
Skill controller routs
 */
const skillsController = require('../controllers/back/skillsController');
router.post('/api/skill', skillsController.store);
router.put('/api/skill/:id', skillsController.update);
router.get('/api/skills', skillsController.getAll);
router.get('/api/skill/:id', skillsController.get);
router.delete('/api/skill/:id', skillsController.delete);
/*
Users controller routs
 */
const usersController = require('../controllers/back/usersController');
router.post('/api/user', usersController.store);
router.put('/api/user/:id', usersController.update);
router.get('/api/users', usersController.getAll);
router.get('/api/user/:id', usersController.get);
router.delete('/api/user/:id', usersController.delete);
router.post('/api/login', usersController.login);
router.get('/api/current', auth.required, usersController.current);
router.post('/api/logout', usersController.logout);
/*
Index controller routs
 */
const indexController = require('../controllers/back/indexController');
router.get('/back/login', indexController.login);
router.get('/back', indexController.index);
router.get('/back/users', indexController.index);
router.get('/back/positions', indexController.index);
router.get('/back/departments', indexController.index);
router.get('/back/skills', indexController.index);

// export module
module.exports = router;
