//Default model require
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

//Model description
var ModelSchema = new Schema(
    {
        first_name: {
            type: String,
            //Validator with message
            required: [true, 'Where is first name?'],
            min: 1,
            max: 255
        },
        last_name: {
            type: String,
            //Validator with message
            required: [true, 'Where is last name?'],
            min: 1,
            max: 255
        },
        email: {
            type: String,
            index: true,
            unique: true,
            required: true,
            match: [
                /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                'Please fill a valid email address'
            ]
        },
        role: {
            type: Number,
            enum: [process.env.ROLE_ADMIN, process.env.ROLE_MANAGER, process.env.ROLE_USER],
            default: process.env.ROLE_USER
        },
        user_department: {
            required: [true, 'Where is department?'],
            type: Schema.Types.ObjectId,
            ref: 'department'
        },
        user_position: {
            required: [true, 'Where is position?'],
            type: Schema.Types.ObjectId,
            ref: 'position'
        },
        user_skills: [
            {
                type: Schema.Types.ObjectId,
                ref: 'skill'
            }
        ],
        hash: {
            type: String,
        },
        salt: {
            type: String,
        },
        created_at: {
            type: [Date, 'Wrong date format'],
            default: Date.now
        },
        updated_at: {
            type: [Date, 'Wrong date format'],
            default: Date.now
        }
    }
);
ModelSchema.plugin(uniqueValidator, {message: 'Email is already taken.'});

ModelSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
ModelSchema.methods.generateJWT = function () {
    const today = new Date();
    const expirationDate = new Date(today);
    // + 60 days expire time
    //expirationDate.setDate(today.getDate() + 60);
    return jwt.sign({
        email: this.email,
        id: this._id,
        // + 2 minutes expire time
        exp: parseInt((expirationDate.getTime() + 120000) / 1000, 10),
    }, 'secret');
};
ModelSchema.methods.validatePassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

ModelSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        email: this.email,
        token: this.generateJWT(),
    };
};
//Create object
module.exports = mongoose.model('UserModel', ModelSchema);