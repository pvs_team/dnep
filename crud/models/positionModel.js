//Default model require
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Model description
var ModelSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, 'Where is name?'],
            min: [1, 'Minimal length 1'],
            max: [255, 'Maximum length 255']
        },
        created_at: {
            type: [Date, 'Wrong date format'],
            default: Date.now
        },
        updated_at: {
            type: [Date, 'Wrong date format'],
            default: Date.now
        }
    }
);
//Create object
module.exports = mongoose.model('PositionModel', ModelSchema);