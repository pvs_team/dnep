import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import Login from './Login.vue';

Vue.use(VueRouter);

import DepartmentComponent from './components/Departments.vue';
import PositionsComponent from './components/Positions.vue';
import SkillsComponent from './components/Skills.vue';
import UsersComponent from './components/Users.vue';

const routes = [
    {
        name: 'Home',
        path: '/back/',
        component: App
    },
    {
        name: 'Users',
        path: '/back/users',
        component: UsersComponent
    },
    {
        name: 'Departments',
        path: '/back/departments',
        component: DepartmentComponent
    },
    {
        name: 'Positions',
        path: '/back/positions',
        component: PositionsComponent
    },
    {
        name: 'Skills',
        path: '/back/skills',
        component: SkillsComponent
    }
];

const router = new VueRouter({mode: 'history', routes: routes});

Vue.config.productionTip = false;

new Vue(
    {
        render: h => h(Login),
    }).$mount('#login-app');

new Vue(Vue.util.extend({router}, App), {
    router: router,
    render: h => h(App),
}).$mount('#app');

