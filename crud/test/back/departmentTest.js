let model = require('../../models/departmentModel');
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../../app');
let should = chai.should();

chai.use(chaiHttp);
const testingInstance = 'department';
const testingInstances = 'departments';

describe('Department >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', () => {
    beforeEach((done) => {
        model.remove({}, (err) => {
            done();
        });
    });
    describe(' /POST ' + testingInstance, () => {
        //Check POST error
        it('---------------------- it not POST a ' + testingInstance + ' without field(s)', (done) => {
            let data = {};
            chai.request(app)
                .post('/api/' + testingInstance)
                .send(data)
                .end((err, res) => {
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.name.should.have.property('kind').eql('required');
                    done();
                });
        });
        //Check POST successful
        it('++++++++++++++++++++++++ it POST a ' + testingInstance, (done) => {
            let data = {
                name: "Development",
            };
            chai.request(app)
                .post('/api/' + testingInstance)
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Successfully added!');
                    res.body.department.should.have.property('name');
                    done();
                });
        });
    });
    //Check PUT successful
    describe('/PUT ' + testingInstances, () => {
        it('+++++++++++++++++++++++ it PUT ' + testingInstances, (done) => {
            let testData = new model({name: "Test update " + testingInstance});
            testData.save((err, returnObject) => {
                chai.request(app)
                    .put('/api/' + testingInstance + '/' + returnObject._id)
                    .send({name: 'New test name', updated_at: Date.now()})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Successfully updated!');
                        res.body.department.should.have.property('name').eql('New test name');
                        done();
                    });
            });
        });
    });
    //Check GET all
    describe('/GET ' + testingInstances, () => {
        it('+++++++++++++++++++++++ it GET all ' + testingInstances, (done) => {
            chai.request(app)
                .get('/api/' + testingInstances)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });
    //Check GET one
    describe('/GET/:id ' + testingInstance, () => {
        it('+++++++++++++++++++++++++ it GET one ' + testingInstance + ' by the id', (done) => {
            let testData = new model({name: "Test " + testingInstance});
            testData.save((err, returnObject) => {
                chai.request(app)
                    .get('/api/' + testingInstance + '/' + returnObject._id)
                    .send(returnObject)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name');
                        res.body.should.have.property('created_at');
                        res.body.should.have.property('updated_at');
                        res.body.should.have.property('_id').eql(returnObject.id);
                        done();
                    });
            });
        });
    });
    //Check DELETE one
    describe('/GET/:id ' + testingInstance, () => {
        it('+++++++++++++++++++++++++ it DELETE one ' + testingInstance + ' by the id', (done) => {
            let testData = new model({name: "Test Delete" + testingInstance});
            testData.save((err, returnObject) => {
                chai.request(app)
                    .delete('/api/' + testingInstance + '/' + returnObject._id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Successfully removed!');
                        done();
                    });
            });
        });
    });
});