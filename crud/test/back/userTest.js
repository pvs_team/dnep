let model = require('../../models/userModel');
let modelPosition = require('../../models/positionModel');
let modelDepartment = require('../../models/departmentModel');
let modelSkill = require('../../models/skillModel');
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../../app');
let should = chai.should();

chai.use(chaiHttp);
const testingInstance = 'user';
const testingInstances = 'users';

describe('User >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', () => {
    beforeEach((done) => {
        model.remove({}, (err) => {
            done();
        });
    });
    describe(' /POST ' + testingInstance, () => {
        //Check POST error
        it('---------------------- it not POST a ' + testingInstance + ' without field(s)', (done) => {
            let data = {};
            chai.request(app)
                .post('/api/' + testingInstance)
                .send(data)
                .end((err, res) => {
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('first_name');
                    res.body.errors.should.have.property('last_name');
                    res.body.errors.first_name.should.have.property('kind').eql('required');
                    res.body.errors.last_name.should.have.property('kind').eql('required');
                    done();
                });
        });
        //Check POST successful
        it('++++++++++++++++++++++++ it POST a ' + testingInstance, (done) => {
            let position = new modelPosition({name: "Junior"});
            position.save();
            let department = new modelDepartment({name: "Manager"});
            department.save();
            let data = {
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Junior',
                last_name: testingInstance + 'Manager',
                role: process.env.ROLE_MANAGER,
                user_position: position,
                user_department: department,
            };
            chai.request(app)
                .post('/api/' + testingInstance)
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Successfully added!');
                    res.body.user.should.have.property('first_name').eql(testingInstance + 'Junior');
                    res.body.user.should.have.property('last_name').eql(testingInstance + 'Manager');
                    res.body.user.should.have.property('role').eql(parseInt(process.env.ROLE_MANAGER));
                    res.body.user.should.have.property('user_position').eql(position.id);
                    res.body.user.should.have.property('user_department').eql(department.id);
                    done();
                });

        });
        //Check POST successful
        it('------------------------ it POST a ' + testingInstance + 'with the same email', (done) => {
            let position = new modelPosition({name: "Junior"});
            position.save();
            let department = new modelDepartment({name: "Manager"});
            department.save();
            let data = {
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Junior',
                last_name: testingInstance + 'Manager',
                role: process.env.ROLE_MANAGER,
                user_position: position,
                user_department: department,
            };
            model(data).save();
            chai.request(app)
                .post('/api/' + testingInstance)
                .send(data)
                .end((err, res) => {
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.email.should.have.property('message').eql('Email is already taken.');
                    done();
                });

        });
    });
    //Check PUT successful
    describe('/PUT ' + testingInstances, () => {
        it('+++++++++++++++++++++++ it PUT ' + testingInstances, (done) => {
            let position = new modelPosition({name: "Middle"});
            position.save();
            let department = new modelDepartment({name: "Designer"});
            department.save();
            let skill1 = new modelSkill({name: "VueJS"});
            let skill2 = new modelSkill({name: "ReactJs"});
            skill1.save();
            skill2.save();
            let testData = new model({
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Fn',
                last_name: testingInstance + 'Ln',
                role: process.env.ROLE_ADMIN,
                user_position: position,
                user_department: department,
                user_skills: [skill1, skill2]

            });
            testData.save((err, returnObject) => {
                chai.request(app)
                    .put('/api/' + testingInstance + '/' + returnObject._id)
                    .send(
                        {
                            email: testingInstance + '@mail.com',
                            first_name: testingInstance + 'Middle',
                            last_name: testingInstance + 'Designer',
                            updated_at: Date.now()
                        })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Successfully updated!');
                        res.body.user.should.have.property('first_name').eql(testingInstance + 'Middle');
                        res.body.user.should.have.property('last_name').eql(testingInstance + 'Designer');
                        res.body.user.should.have.property('role').eql(parseInt(process.env.ROLE_ADMIN));
                        res.body.user.should.have.property('user_position').eql(position.id);
                        res.body.user.should.have.property('user_department').eql(department.id);
                        res.body.user.should.have.property('user_skills').eql([skill1.id, skill2.id]);
                        done();
                    });
            });
        });
    });
    //Check GET all
    describe('/GET ' + testingInstances, () => {
        it('+++++++++++++++++++++++ it GET all ' + testingInstances, (done) => {
            chai.request(app)
                .get('/api/' + testingInstances)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });
    //Check GET one
    describe('/GET/:id ' + testingInstance, () => {
        it('+++++++++++++++++++++++++ it GET one ' + testingInstance + ' by the id', (done) => {
            let position = new modelPosition({name: "Middle"});
            position.save();
            let department = new modelDepartment({name: "Designer"});
            department.save();
            let testData = new model({
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Middle',
                last_name: testingInstance + 'Designer',
                user_position: position,
                user_department: department
            });
            testData.save((err, returnObject) => {
                chai.request(app)
                    .get('/api/' + testingInstance + '/' + returnObject._id)
                    .send(returnObject)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('first_name');
                        res.body.should.have.property('last_name');
                        res.body.should.have.property('created_at');
                        res.body.should.have.property('updated_at');
                        res.body.should.have.property('_id').eql(returnObject.id);
                        done();
                    });
            });
        });
    });
    //Check DELETE one
    describe('/GET/:id ' + testingInstance, () => {
        it('+++++++++++++++++++++++++ it DELETE one ' + testingInstance + ' by the id', (done) => {
            let position = new modelPosition({name: "Senior"});
            position.save();
            let department = new modelDepartment({name: "Backend"});
            department.save();
            let testData = new model({
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Senior',
                last_name: testingInstance + 'Backend',
                user_position: position,
                user_department: department
            });
            testData.save((err, returnObject) => {
                chai.request(app)
                    .delete('/api/' + testingInstance + '/' + returnObject._id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Successfully removed!');
                        done();
                    });
            });
        });
    });
});