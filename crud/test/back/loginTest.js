let model = require('../../models/userModel');
let modelPosition = require('../../models/positionModel');
let modelDepartment = require('../../models/departmentModel');
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../../app');
let should = chai.should();

chai.use(chaiHttp);
const testingInstance = 'login';

describe('User >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', () => {
    beforeEach((done) => {
        model.remove({}, (err) => {
            done();
        });
    });
    //Check LOGIN one
    describe('api login ' + testingInstance, () => {
        let position = new modelPosition({name: "MiddleLogin"});
        position.save();
        let department = new modelDepartment({name: "DesignerLogin"});
        department.save();
        it('+++++++++++++++++++++++++ it LOGIN one ' + testingInstance + ' by the email and password', (done) => {
            let testData = new model({
                email: testingInstance + '@mail.com',
                first_name: testingInstance + 'Login',
                last_name: testingInstance + 'Login',
                user_position: position,
                user_department: department
            });
            testData.save((err, returnObject) => {
                let password = 'Some password';
                returnObject.setPassword(password);
                returnObject.save((err, returnObject) => {
                    chai.request(app)
                        .post('/api/login')
                        .send({user: {email: returnObject.email, password: password}})
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.user.should.have.property('_id').eql(returnObject.id);
                            res.body.user.should.have.property('email').eql(returnObject.email);
                            done();
                        });
                });

            });
        });
    });
    describe('api login ' + testingInstance, () => {
        let position = new modelPosition({name: "MiddleLogin"});
        position.save();
        let department = new modelDepartment({name: "DesignerLogin"});
        department.save();
        it('------------------------ it LOGIN one ' + testingInstance + ' with wrong password', (done) => {
            let testData = new model({
                email: testingInstance + '@failMail.com',
                first_name: testingInstance + 'Login',
                last_name: testingInstance + 'Login',
                user_position: position,
                user_department: department
            });
            testData.save((err, returnObject) => {
                let password = 'Some password';
                returnObject.setPassword(password);
                returnObject.save((err, returnObject) => {
                    chai.request(app)
                        .post('/api/login')
                        .send({user: {email: returnObject.email, password: password + 'wrong'}})
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('errors');
                            res.body.errors.should.have.property('email or password').eql('is invalid');
                            done();
                        });
                });
            });
        });
    });
});