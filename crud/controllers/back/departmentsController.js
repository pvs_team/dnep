//Use
const departmentModel = require('../../models/departmentModel');
//Methods
//Crete new
exports.store = function (req, res) {
    var newDepartment = new departmentModel(req.body);
    newDepartment.save((err, department) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully added!", department});
        }

    });
};
//Update
exports.update = function (req, res) {
    departmentModel.findById(req.params.id, (err, department) => {
        if (err) {
            res.send(err);
        } else {
            Object.assign(department, req.body).save((err, department) => {
                if (err) {
                    res.send(err);
                } else {
                    res.json({message: "Successfully updated!", department});
                }

            });
        }
    });
};
//Get all
exports.getAll = function (req, res) {
    departmentModel.find({}, (err, departments) => {
        if (err) {
            res.send(err);
        } else {
            res.json(departments);
        }
    });
};
//Get one
exports.get = function (req, res) {
    departmentModel.findById(req.params.id, (err, department) => {
        if (err) {
            res.send(err);
        } else {
            res.json(department);
        }
    });
};
//Delete one
exports.delete = function (req, res) {
    departmentModel.remove({_id: req.params.id}, (err) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully removed!"});
        }
    });
};