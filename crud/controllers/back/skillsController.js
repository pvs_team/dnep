//Use
const skillModel = require('../../models/skillModel');
//Methods
//Crete new
exports.store = function (req, res) {
    var newSkill = new skillModel(req.body);
    newSkill.save((err, skill) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully added!", skill});
        }

    });
};
//Update
exports.update = function (req, res) {
    skillModel.findById(req.params.id, (err, skill) => {
        if (err) {
            res.send(err);
        } else {
            Object.assign(skill, req.body).save((err, skill) => {
                if (err) {
                    res.send(err);
                } else {
                    res.json({message: "Successfully updated!", skill});
                }

            });
        }
    });
};
//Get all
exports.getAll = function (req, res) {
    skillModel.find({}, (err, skills) => {
        if (err) {
            res.send(err);
        } else {
            res.json(skills);
        }
    });
};
//Get one
exports.get = function (req, res) {
    skillModel.findById(req.params.id, (err, skill) => {
        if (err) {
            res.send(err);
        } else {
            res.json(skill);
        }
    });
};
//Delete one
exports.delete = function (req, res) {
    skillModel.remove({_id: req.params.id}, (err) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully removed!"});
        }
    });
};