//Use
const positionModel = require('../../models/positionModel');
//Methods
//Crete new
exports.store = function (req, res) {
    var newPosition = new positionModel(req.body);
    newPosition.save((err, position) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully added!", position});
        }

    });
};
//Update
exports.update = function (req, res) {
    positionModel.findById(req.params.id, (err, position) => {
        if (err) {
            res.send(err);
        } else {
            Object.assign(position, req.body).save((err, position) => {
                if (err) {
                    res.send(err);
                } else {
                    res.json({message: "Successfully updated!", position});
                }

            });
        }
    });
};
//Get all
exports.getAll = function (req, res) {
    positionModel.find({}, (err, postions) => {
        if (err) {
            res.send(err);
        } else {
            res.json(postions);
        }
    });
};
//Get one
exports.get = function (req, res) {
    positionModel.findById(req.params.id, (err, position) => {
        if (err) {
            res.send(err);
        } else {
            res.json(position);
        }
    });
};
//Delete one
exports.delete = function (req, res) {
    positionModel.remove({_id: req.params.id}, (err) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully removed!"});
        }
    });
};