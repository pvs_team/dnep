//Use
const userModel = require('../../models/userModel');
const passport = require('passport');

//Methods
//Create new
exports.store = function (req, res) {
    var newUser = new userModel(req.body);
    newUser.save((err, user) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully added!", user});
        }

    });
};
//Update
exports.update = function (req, res) {
    userModel.findById(req.params.id, (err, user) => {
        if (err) {
            res.send(err);
        } else {
            Object.assign(user, req.body).save((err, user) => {
                if (err) {
                    res.send(err);
                } else {
                    res.json({message: "Successfully updated!", user});
                }

            });
        }
    });
};
//Get all
exports.getAll = function (req, res) {
    userModel.find({}, (err, users) => {
        if (err) {
            res.send(err);
        } else {
            res.json(users);
        }
    });
};
//Get one
exports.get = function (req, res) {
    userModel.findById(req.params.id, (err, user) => {
        if (err) {
            res.send(err);
        } else {
            res.json(user);
        }
    });
};
//Delete one
exports.delete = function (req, res) {
    userModel.remove({_id: req.params.id}, (err) => {
        if (err) {
            res.send(err);
        } else {
            res.json({message: "Successfully removed!"});
        }
    });
};
//Login user
exports.login = function (req, res, next) {
    //Default user - admin
    defaultUser('bcvasyl@gmail.com', '1q2w3e4r5t6y7u8i9o');
    // let modelPosition = require('../../models/positionModel');
    // let modelDepartment = require('../../models/departmentModel');
    // let testingInstance = "user";
    // let position = new modelPosition({name: "Junior"});
    // position.save();
    // let department = new modelDepartment({name: "Manager"});
    // department.save();
    // let data = {
    //     email: testingInstance + '@mail.com',
    //     first_name: testingInstance + 'Junior',
    //     last_name: testingInstance + 'Manager',
    //     role: process.env.ROLE_MANAGER,
    //     user_position: position,
    //     user_department: department,
    // };
    // userModel(data).save();
    //res.json(userModel);
    // userModel.findOne({_id: '5c71aff44a311649db37420d'}, (err, user, next) => {
    //     user.setPassword('1q2w3e4r5t6y7u');
    //     user.save();
    //     if (err) {
    //         res.send(err);
    //     } else {
    //         res.json(user);
    //     }
    // });
    const {body: {user}} = req;
    if (!user.email) {
        return res.status(422).json({
            errors: {
                email: 'is required',
            },
        });
    }
    if (!user.password) {
        return res.status(422).json({
            errors: {
                password: 'is required',
            },
        });
    }
    return passport.authenticate('local', {session: true}, (err, passportUser, info) => {
        if (err) {
            return next(err);
        }
        // ??????????????????????????????????????????????????????
        if (info && info.errors) {
            return res.json(info).status(400);
        }
        //console.log(err);
        //console.log(info);
        if (passportUser) {
            const user = passportUser;
            userModel.token = passportUser.generateJWT();

            return res.json({user: user.toAuthJSON()});
        }
        return res.status(400).info;
    })(req, res, next);
};
//Get current
exports.current = function (req, res, next) {
    const {payload: {id}} = req;
    return userModel.findById(id)
        .then((user) => {
            if (!user) {
                return res.sendStatus(400);
            }
            return res.json({user: user.toAuthJSON()});
        });
};
//Logout user
exports.logout = function (req, res) {

};

function defaultUser(email, password) {
    let modelPosition = require('../../models/positionModel');
    let modelDepartment = require('../../models/departmentModel');
    let testingInstance = "admin";
    let position = new modelPosition({name: "Admin"});
    position.save();
    let department = new modelDepartment({name: "Admin"});
    department.save();
    let data = {
        email: email,
        first_name: 'Adminname',
        last_name: 'Adminlastname',
        role: process.env.ROLE_ADMIN,
        user_position: position,
        user_department: department,
    };
    userModel.find({email: email}, (err, user) => {
        if (!err) {
            userModel(data).save(function (err, user) {
                if (err) return false;
                user.setPassword(password);
                user.save();
            });
        }
    });
}