$(function () {
    var socket = io();
    var date = new Date();
    myStorage = window.localStorage;
    //Open subscribe modal
    if (myStorage.getItem('socket_id') == null) {
        $(document).ready(function () {
            $('#modalSubscribe').modal('show');
        });
        socket.emit('update_users');
    }
    $('#subscribe_form').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('update_users',
            $('#usernameInput').val().trim()
        );
        $('#modalSubscribe').modal('show');
        myStorage.setItem($('#usernameInput').val().trim(), socket.id);
        // socket.emit('subscribe',
        //     $('#usernameInput').val().trim()
        // );
    });
    // socket.on('save_in_storage', function (name, id) {
    //     myStorage.setItem(name, id);
    //     //$('#modalSubscribe').modal('hide');
    // });
    socket.on('update_users', function (users) {
        $('.inbox_chat').html('');
        //console.log(users);
        for (var index in users) {
            if (users[index] !== undefined) {
                var online = users[index].online
                    ? $('<strong style="color: darkgreen">Online</strong>')
                    : $('<strong style="color: indianred">Offline</strong>');
                var typing = users[index].typing != undefined && users[index].typing
                    ? $('<p>Typing...</p>')
                    : $('<p></p>');
                $('.inbox_chat')
                    .append($('<div class="chat_list active_chat">')
                        .append($('<div class="chat_people">')
                            .append($('<div class="chat_img">')
                                .append($('<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">')
                                )
                            ).append($('<div class="chat_ib">')
                                .append($('<h5>' + index + '</h5>'))
                                .append(online)
                                .append(typing)
                            )
                        )
                    );
                //console.log(users[index]);
            }
        }
        //console.log(users);
    });
    $('form').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        if ($('#m').val().trim() == '') {
            return false;
        }
        socket.emit('chat message',
            $('#m').val(),
            (date.getHours() < 10 ? '0' + date.getHours() : date.getHours())
            + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
            + ':' + (date.getSeconds() < 10 ? '0' + date.getMinutes() : date.getMinutes())
        );
        $('#m').val('');
        return false;
    });
    $('#m').keydown(function () {
        socket.emit('typing', true);
        setTimeout(function () {
            socket.emit('typing', false);
        }, 5000);
    });

    socket.on('chat message', function (msg, time) {
        //console.log(msg);
        $('.msg_history')
            .append($('<div class="incoming_msg">')
                .append($('<div class="incoming_msg_img">')
                    .append($('<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">'))
                )
                .append($('<div class="received_msg">')
                    .append($('<div class="received_withd_msg">')
                        .append($('<p>' + msg + '</p>'))
                        .append($('<span class="time_date">' + time + '</span>'))
                    )
                )
            );
    });
});