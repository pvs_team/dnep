var app = require('express')();
var http = require('http').Server(app);
var express = require('express');
var io = require('socket.io')(http);
var redis = require('redis');
var bodyParser = require('body-parser');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function () {
    console.log('listening on *:3000');
});

var chat_users = {};
var chat_messages = {};
// Redis Client Ready
client = redis.createClient('redis://127.0.0.1:6379');
client.once('ready', function () {
    // Initialize chat users
    client.get('chat_users', function (err, reply) {
        console.log(reply);
        if (reply) {
            chat_users = JSON.parse(reply);
        }
    });
    // Initialize Messages
    client.get('chat_messages', function (err, reply) {
        if (reply) {
            chat_messages = JSON.parse(reply);
        }
    });
});
console.log('===============================================');

//+++++++++++++++++++++++Socket.io++++++++++++++++++++++++++++++++++++++
io.on('connection', function (socket) {
    socket.on('message', function (data) {
        io.emit('send', data);
    });
    socket.on('update_users', function (data) {
        console.log(data);
        client.get('chat_users', function (err, reply) {
            console.log(reply);
            if (reply) {
                chat_users = JSON.parse(reply);
            }
        });
        if (data != undefined && chat_users[data] == undefined) {
            chat_users[data] = {'id': socket.id, 'online': true};
            client.set('chat_users', JSON.stringify(chat_users));
            io.emit('update_users', chat_users);
        }
    });
    socket.on('typing', function (typing) {
        console.log(socket.id);
        for (var index in chat_users) {
            if (chat_users[index]['id'] == socket.id) {
                chat_users[index]['typing'] = typing;
            }
        }
        io.emit('update_users', chat_users);
    });
    socket.on('chat message', function (msg, time) {
        for (var index in chat_users) {
            if (chat_users[index]['id'] == socket.id) {
                chat_messages = {'username': index, 'message': msg, 'time': time};
            }
        }
        client.set('chat_messages', JSON.stringify(chat_messages));
        io.emit('chat message', msg, time);
    });
    socket.on('disconnect', function () {
        io.emit('update_users', chat_users);
    });
});